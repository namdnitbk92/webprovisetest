<?php
namespace App\Helpers;

class Company {
    use Helper;

    private $id;
    private $name;
    private $parentId;
    private $createdAt;
    private $cost = 0;
    private $children = [];

    function __construct($company) {
        extract($company);
        $this->setId($id);
        $this->setCreatedAt($createdAt);
        $this->setName($name);
        $this->setParentId($parentId);
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of parentId
     */ 
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set the value of parentId
     *
     * @return  self
     */ 
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get the value of createdAt
     */ 
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set the value of createdAt
     *
     * @return  self
     */ 
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get the value of cost
     */ 
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set the value of cost
     *
     * @return  self
     */ 
    public function setCost($cost)
    {
        $this->cost = $cost;
        
        return $this;
    }

    public function getChildrenToArray()
    {
        return array_map(function ($child) {
            return $child->toArray();
        }, $this->getChildren());
    }

    /**
     * Get the value of children
     */ 
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set the value of children
     *
     * @return  self
     */ 
    public function setChildren($companies)
    {
        $this->children = $this->getChilds($companies);
        return $this;
    }

    public function getChilds($companies)
    {
        return array_filter($companies, function ($company) {
            return $company->getParentId() == $this->getId();
        });
    }

    public function hasChildren()
    {
        return count($this->children) > 0;
    }

    public function calcCost($companies, $travels) 
    {
        $costItems = array_filter($travels, function ($travel) {
            return $travel->getCompanyId() === $this->getId();
        });
        
        //cost of company in travels list
        $cost = array_reduce($costItems, function ($cost, $item) {
            $cost += (int) $item->getPrice();
            return $cost;
        }, 0);

        //calc cost of childs
        $costOfChild = 0;
        $costOfChild = array_reduce($this->getChildren(), function ($costOfChild, $child) use ($companies, $travels) {
            $costOfChild += (int) $child->setChildren($companies)->calcCost($companies, $travels)->getCost();
            return $costOfChild;
        }, 0);

        $this->setCost($cost + $costOfChild);
        return $this;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'cost' => $this->getCost(),
            'children' => $this->getChildrenToArray()
        ];
    }
}