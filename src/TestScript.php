<?php 

namespace App\Helpers;

const TRAVEL_URL = 'https://5f27781bf5d27e001612e057.mockapi.io/webprovise/travels';
const COMPANY_URL = 'https://5f27781bf5d27e001612e057.mockapi.io/webprovise/companies';
const PARENT_ID_VALUE = "0";

class TestScript
{
    use Helper;

    private $tree = [];
    private $companies = [];
    private $travels = [];

    public function __construct() {
        $this->setCompanies($this->callApi(COMPANY_URL));
        $this->setTravels($this->callApi(TRAVEL_URL));
    }

    public function calcTree() {
        $tree = [];
        $companies = $this->getCompanies();
        $travels = $this->getTravels(); 
        
        $tree = $companies[0]
            ->setChildren($companies)
            ->calcCost($companies, $travels)
            ->toArray();
            
        $this->setTree($tree);
        return $this;
    }

    public function execute()
    {
        $start = microtime(true);
        $tree = $this->calcTree()->getTree();
        echo json_encode($tree);
        echo 'Total time: '.  (microtime(true) - $start);
    }

    /**
     * Get the value of companies
     */ 
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * Set the value of companies
     *
     * @return  self
     */ 
    public function setCompanies($companies)
    {
        $companiesInstances = [];
        foreach ($companies as $company) {
            $companiesInstances[] = new Company((array) $company);
        }
        
        $this->companies = $companiesInstances;

        return $this;
    }

    /**
     * Get the value of travels
     */ 
    public function getTravels()
    {
        return $this->travels;
    }

    /**
     * Set the value of travels
     *
     * @return  self
     */ 
    public function setTravels($travels)
    {
        $travelsInstances = [];
        foreach ($travels as $travel) {
            $travelsInstances[] = new Travel((array) $travel);
        }
        
        $this->travels = $travelsInstances;

        return $this;
    }

    /**
     * Get the value of tree
     */ 
    public function getTree()
    {
        return $this->tree;
    }

    /**
     * Set the value of tree
     *
     * @return  self
     */ 
    public function setTree($tree)
    {
        $this->tree = $tree;

        return $this;
    }
}


?>